import scala.io.Source

case class Student(pesel: String, lastName: String, firstName: String)

case class Registration(pesel: String, room: String)

case class Borrowing(no: String, pesel: String, title: String)

object Zad5 {


  def main(args: Array[String]) {
    val students = readFile("../Dane_PR2/studenci.txt").map(a => Student(a(0), a(1), a(2))).drop(1)
    val registrations = readFile("../Dane_PR2/meldunek.txt").map(a => Registration(a(0), a(1))).drop(1)
    val borrowings = readFile("../Dane_PR2/wypozyczenia.txt").map(a => Borrowing(a(0), a(1), a(2))).drop(1)


    println("5.1")

    val studentsMap2 = students.groupBy(_.pesel)
    val booksPerStudent = borrowings.groupBy(_.pesel)
    val (pesel, borrowedBooks) = booksPerStudent.maxBy(_._2.size)
    val topStudent :: _ = studentsMap2(pesel)
    println(topStudent, borrowedBooks.mkString("\n"))


    println("5.2")

    val peoplePerRoom = registrations.groupBy(_.room).mapValues(_.size)
    val roomCount = peoplePerRoom.size
    val totalOfRegistrations = registrations.size
    println("%1.4f".format(1.0 * totalOfRegistrations / roomCount))

    println("5.3")

    def isMan = (s: Student) => s.pesel.charAt(s.pesel.length - 2).toInt % 2 != 0
    val men = students.filter(isMan).size
    val women = students.filterNot(isMan).size
    println("students ", students.size)
    println("men ", men)
    println("women ", women)

    println("5.4")
    def isFromDorm = (s: Student) => registrations.find(r => r.pesel == s.pesel).isDefined
    val notInDorm = students.filterNot(isFromDorm)
    println(notInDorm.sortBy(_.lastName).mkString("\n"))

    println("5.5") //316
    val peselToRoomMap2 = registrations.groupBy(_.pesel).mapValues(_ (0).room)
    val enhancedBorrowings2 = borrowings.map(b => new {
      val title = b.title;
      val rooms = peselToRoomMap2.getOrElse(b.pesel, -1)
    })
    val borrowingToRoomMap2 = enhancedBorrowings2.groupBy(_.title).mapValues(_.map(_.rooms))
    val withoutDuplicates2 = borrowingToRoomMap2.map(t => t._2.toSet).flatten.size
    println(withoutDuplicates2)
  }

  def readFile(fileName: String): List[Array[String]] = {
    val lines = Source.fromFile(fileName).getLines()
    val students = lines.map(_.split("\t")).toList
    students
  }
}
