import scala.io.Source


object Zad6_3 {

  def isCorrect(en: String, de: String): Boolean = {
    def d = (c1: Char, c2: Char) => Math.floorMod(c2.toInt - c1.toInt, 26)
    val diffs = en.zip(de).map(t => d(t._1, t._2))
    diffs.toSet.size == 1
  }

  /*
    ZAWISLAK EFBNXQFP

     */
  def main(args: Array[String]) {


    val words = readFile("../Dane_PR2/dane_6_3.txt")
      .map(l => l.split(" "))

    val f = words.filterNot(p => isCorrect(p(0), p(1)))
    println(f.map(_(0)).mkString("\n"))

  }

  def readFile(fileName: String): List[String] = {
    return Source.fromFile(fileName).getLines().toList
  }
}
