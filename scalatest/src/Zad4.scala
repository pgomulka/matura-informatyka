import scala.io.Source


object Zad4 {
  def isOnEdge(coordinate: Array[Int]): Boolean = {
    val Array(x, y) = coordinate
    (x - 200) * (x - 200) + (y - 200) * (y - 200) == 200 * 200
  }

  def isInCircle(coordinate: Array[Int]): Boolean = {
    val Array(x, y) = coordinate
    (x - 200) * (x - 200) + (y - 200) * (y - 200) < 200 * 200
  }

  def Pk(x: Int): Double = x * x * Math.PI

  def P(x: Int): Double = x * x

  /*
    nk / n = pk/ p
    nk/ n = PI*r*r / (2r*2r)
    nk/n *4= PI

   */

  def pi(n: Int, coordinates: List[Array[Int]]): Double = {
    var nk = 0
    for (i <- 0 to n - 1) {

      if (isInCircle(coordinates(i))) {
        nk = nk + 1
      }
    }
    nk * 4.0 / n
  }


  def format(e: Double): String = "%.4f".format(e)

  def main(args: Array[String]) {
    val coordinates = loadLines
    println(4.1)
    val inCircle = coordinates.filter(isInCircle).size
    println("in circle ", inCircle)
    val onEdge = coordinates.filter(isOnEdge)
    println("on edge ", onEdge.map(_.mkString(",")).mkString(" "))

    println(4.2)
    println(pi(1000, coordinates))
    println(pi(5000, coordinates))
    println(pi(coordinates.size, coordinates))

    println(4.3)
    for (i <- 1 to 1700) {
      val e = (Math.PI - pi(i, coordinates)).abs
      println(i + "\t" + format(e))
    }


  }

  def loadLines: List[Array[Int]] = {
    val filename = "../Dane_PR2/punkty.txt"
    val lines = Source.fromFile(filename).getLines()
    return lines.map(lineToCoordinates).toList
  }

  def lineToCoordinates: (String) => Array[Int] = {
    _.split(" ").map(_.toInt)
  }
}
