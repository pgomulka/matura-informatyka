import scala.io.Source


object Zad6 {


  def shift(c: Char, k: Int): Char = {
    (Math.floorMod(c.toInt - 65 + k, 26) + 65).toChar
  }

  def encrypt(k: Int): (String) => String = {
    (w: String) => w.map(c => shift(c, k))
  }

  /*
  A 65
  Z 90
  LQWHUSUHWRZDQLH
   */
  def main(args: Array[String]) {

    //    println("6.1")
    //    val words = readFile("../Dane_PR2/dane_6_1.txt")
    //    val encrypted = words.map(encrypt(107))
    //    println(encrypted.mkString("\n"))

    //    println("6.2")
    val words2 = readFile("../Dane_PR2/dane_6_2.txt")
      .filter(l => l.split(" ").size == 2)
      .map(l => (l.split(" ")(0), l.split(" ")(1).toInt))
    val decrypted = words2.map((t) => encrypt(-t._2)(t._1))
    println(decrypted.mkString("\n"))

  }

  def readFile(fileName: String): List[String] = {
    return Source.fromFile(fileName).getLines().toList
  }
}
